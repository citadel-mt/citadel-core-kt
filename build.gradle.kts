import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.jetbrains.kotlin.jvm").version("1.3.61")
    `java-library`
    `maven-publish`
}

repositories {
    jcenter()
}

val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions.freeCompilerArgs = listOf(
        "-Xuse-experimental=kotlin.Experimental",
        "-Xallow-result-return-type"
)

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.0")

    implementation("com.github.salomonbrys.kotson:kotson:2.5.0")

    testImplementation("com.nhaarman.mockitokotlin2:mockito-kotlin:2.2.0")
    testImplementation("org.jetbrains.kotlin:kotlin-test")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit")
}

val includedLibs = arrayListOf<String>()

tasks.named<Jar>("jar") {
    configurations["compileClasspath"].forEach { file: File ->
        if (file.name in includedLibs) {
            from(zipTree(file.absoluteFile))
        }
    }
}

publishing {
    repositories {
        maven("https://mymavenrepo.com/repo/2adloi83ZbzFqdzrVebr/")
    }
    publications {
        create<MavenPublication>("maven") {
            groupId = "com.appspot.magtech"
            artifactId = "citadel-core"
            version = "0.0.4"

            from(components["kotlin"])
        }
    }
}
