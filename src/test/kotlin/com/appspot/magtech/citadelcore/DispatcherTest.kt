package com.appspot.magtech.citadelcore

import com.appspot.magtech.citadelcore.utils.CitadelEvent
import com.appspot.magtech.citadelcore.utils.ResponsableEvent
import kotlinx.coroutines.*
import org.junit.Test
import kotlin.test.assertEquals

data class TestMessage(val name: String)

class DispatcherTest {

    class ChannelMock : ServerChannel {
        override fun initialize() {
            println("Initialized")
        }

        override val onDeviceConnectedEvent = CitadelEvent<ChannelDependentDevice>()

        override fun terminate() {
            println("Terminated")
        }

        override fun sendMessageAsync(message: String): Deferred<Unit> {
            return CoroutineScope(Dispatchers.IO).async {
                println(message)
                delay(1000)
                onReceiveMessageEvent.notifyAll(
                        """{"type":"RESPONSE","targetId":"Test","payload":{"streamId":"TEST_OUTCOMING","value":{"name":"Test response"}}}"""
                )
                // throw Exception("Test exception")
            }
        }

        override val onReceiveMessageEvent = CitadelEvent<String>()
    }

    @PluginInfo(id = "Test")
    interface TestPlugin : CitadelPlugin {

        @Sender("TEST_OUTCOMING")
        fun sendTestMessageAsync(message: EmptyRequest): Deferred<TestMessage>

        @Receiver("TEST_INCOMING")
        val onReceiveTestEvent: ResponsableEvent<TestMessage, SimpleResponse>
    }

    @Test
    fun testSendMessage() = runBlocking {

        val channel = ChannelMock()

        val plugin = citadelPluginFrom<TestPlugin>(
                channel,
                MessageDispatcher(channel)
        )

        val result = plugin.sendTestMessageAsync(EmptyRequest).await()
        assertEquals(result, TestMessage("Test response"))
        Unit
    }

    @Test
    fun testReceiveMessage() = runBlocking {
        val channel = ChannelMock()

        val plugin = citadelPluginFrom<TestPlugin>(
                channel,
                MessageDispatcher(channel)
        )

        plugin.onReceiveTestEvent.set {
            assertEquals(it, TestMessage("Test call"))
            SimpleResponse("OK")
        }

        channel.onReceiveMessageEvent.notifyAll("""{"type":"REQUEST","targetId":"Test","payload":{"type":"com.appspot.magtech.citadelcore.TestMessage","streamId":"TEST_INCOMING","value":{"name":"Test call"}}}""")
        delay(1000)
    }
}
