package com.appspot.magtech.citadelcore.utils

import kotlin.reflect.KType
import kotlin.reflect.full.primaryConstructor

private var gsonSerializer: Serializer? = null

fun gsonSerializer(): Serializer {
    if (gsonSerializer == null) {
        gsonSerializer = Class.forName("com.appspot.magtech.citadelcore.dependency.GsonSerializer").kotlin
                .primaryConstructor!!.call() as Serializer
    }
    return gsonSerializer!!
}

data class RawData(val id: Int)

interface Serializer {

    fun <T> serialize(data: T): String

    fun deserializeToRaw(str: String): RawData

    fun <T : Any> deserializeRaw(data: RawData, type: KType): T?

    fun <T : Any> getRawValue(data: RawData, propName: String): T?
    fun getRawObject(data: RawData, propName: String): RawData?
}

