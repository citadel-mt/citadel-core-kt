package com.appspot.magtech.citadelcore.utils

class ResponsableEvent<E, R> {

    private var event: ((E) -> R)? = null

    operator fun invoke(element: E): R? = event?.invoke(element)

    fun set(event: (E) -> R) {
        this.event = event
    }

    fun reset() {
        this.event = null
    }
}
