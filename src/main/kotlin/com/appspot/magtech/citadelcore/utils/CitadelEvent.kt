package com.appspot.magtech.citadelcore.utils

class CitadelEvent<T> {

    private val list = arrayListOf<(T) -> Unit>()

    operator fun plusAssign(listener: (T) -> Unit) {
        list.add(listener)
    }

    operator fun minusAssign(listener: (T) -> Unit) {
        list.remove(listener)
    }

    fun notifyAll(element: T) {
        list.forEach {
            it(element)
        }
    }
}
