package com.appspot.magtech.citadelcore

import com.appspot.magtech.citadelcore.utils.CitadelEvent
import com.appspot.magtech.citadelcore.utils.ResponsableEvent
import com.appspot.magtech.citadelcore.utils.Serializer
import com.appspot.magtech.citadelcore.utils.gsonSerializer
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.reflect.KType

data class TypedEvent(val type: KType, val event: CitadelEvent<Any>)

data class TypedResponsableEvent(val type: KType, var event: ResponsableEvent<Any, Any>)

typealias PluginId = String
typealias StreamId = String

class MessageDispatcher(
        channel: CitadelChannel,
        serializer: Serializer = gsonSerializer()
) {
    private val events:
            MutableMap<PluginId, MutableMap<StreamId, TypedEvent>> = mutableMapOf()

    private val responsableEvents:
            MutableMap<PluginId, MutableMap<StreamId, TypedResponsableEvent>> = mutableMapOf()

    init {
        channel.onReceiveMessageEvent += { message ->
            val rawData = serializer.deserializeToRaw(message)
            val type = serializer.getRawValue<String>(rawData, "type")
            val targetId = serializer.getRawValue<PluginId>(rawData, "targetId")
            val payload = serializer.getRawObject(rawData, "payload")
            if (type != null && targetId != null && payload != null) {
                val streamId = serializer.getRawValue<StreamId>(payload, "streamId")
                val valueRaw = serializer.getRawObject(payload, "value")
                if (streamId != null) {
                    when (MessageType.valueOf(type)) {
                        MessageType.REQUEST -> {
                            val typedResponsableEvent = getTypedResponsableEvent(targetId, streamId)
                            if (typedResponsableEvent != null) {
                                val value = if (valueRaw != null) {
                                    serializer.deserializeRaw<Any>(valueRaw, typedResponsableEvent.type)
                                } else null
                                if (value != null) {
                                    val response = typedResponsableEvent.event(value)
                                    if (response != null) {
                                        CoroutineScope(Dispatchers.IO).launch {
                                            channel.sendMessageAsync(serializer.serialize(packMessage(
                                                    response,
                                                    MessageType.RESPONSE,
                                                    targetId,
                                                    streamId
                                            ))).await()
                                        }
                                    }
                                }
                            }
                        }
                        MessageType.RESPONSE -> {
                            val typedEvent = getTypedEvent(targetId, streamId)
                            if (typedEvent != null) {
                                val value = if (valueRaw != null) {
                                    serializer.deserializeRaw<Any>(valueRaw, typedEvent.type)
                                } else null
                                if (value != null) {
                                    typedEvent.event.notifyAll(value)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    fun getEvent(pluginId: PluginId, streamId: StreamId, type: KType): CitadelEvent<Any> {
        if (events[pluginId] == null) {
            events[pluginId] = mutableMapOf()
        }
        if (events.getValue(pluginId)[streamId] == null) {
            events.getValue(pluginId)[streamId] = TypedEvent(type, CitadelEvent())
        }
        return events.getValue(pluginId).getValue(streamId).event
    }

    private fun getTypedEvent(pluginId: PluginId, streamId: StreamId): TypedEvent? {
        if (events[pluginId] == null) {
            events[pluginId] = mutableMapOf()
        }
        return events.getValue(pluginId)[streamId]
    }

    fun getResponsableEvent(pluginId: PluginId, streamId: StreamId, type: KType): ResponsableEvent<Any, Any> {
        if (responsableEvents[pluginId] == null) {
            responsableEvents[pluginId] = mutableMapOf()
        }
        if (responsableEvents.getValue(pluginId)[streamId] == null) {
            responsableEvents.getValue(pluginId)[streamId] = TypedResponsableEvent(type, ResponsableEvent())
        }
        return responsableEvents.getValue(pluginId).getValue(streamId).event
    }

    private fun getTypedResponsableEvent(pluginId: PluginId, streamId: StreamId): TypedResponsableEvent? {
        if (responsableEvents[pluginId] == null) {
            responsableEvents[pluginId] = mutableMapOf()
        }
        return responsableEvents.getValue(pluginId)[streamId]
    }
}
