package com.appspot.magtech.citadelcore

import com.appspot.magtech.citadelcore.utils.ResponsableEvent
import kotlinx.coroutines.Deferred

const val pluginId = "com.appspot.magtech.citadel.Service"

const val clientConnectionStream = "CLIENT_CONNECTION"
const val clientDisconnectionStream = "CLIENT_DISCONNECTION"
const val serverTerminationStream = "SERVER_TERMINATION"

@PluginInfo(id = pluginId)
interface ServiceServerPlugin : CitadelPlugin {

    @Sender(streamId = serverTerminationStream)
    fun sendTerminateAsync(message: EmptyRequest): Deferred<SimpleResponse>

    @Receiver(streamId = clientConnectionStream)
    val clientConnectedEvent: ResponsableEvent<CitadelClient, CitadelServer>

    @Receiver(streamId = clientDisconnectionStream)
    val clientDisconnectedEvent: ResponsableEvent<CitadelClient, SimpleResponse>
}

@PluginInfo(id = pluginId)
interface ServiceClientPlugin : CitadelPlugin {

    @Sender(streamId = clientConnectionStream)
    fun sendHelloAsync(data: CitadelClient): Deferred<CitadelServer>

    @Sender(streamId = clientDisconnectionStream)
    fun sendDisconnectAsync(data: CitadelClient): Deferred<SimpleResponse>

    @Receiver(streamId = serverTerminationStream)
    val serverTerminatedEvent: ResponsableEvent<EmptyRequest, SimpleResponse>
}
