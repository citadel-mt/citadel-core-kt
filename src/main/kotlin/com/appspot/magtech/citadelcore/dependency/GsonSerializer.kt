package com.appspot.magtech.citadelcore.dependency

import com.appspot.magtech.citadelcore.utils.RawData
import com.appspot.magtech.citadelcore.utils.Serializer
import com.github.salomonbrys.kotson.fromJson
import com.google.gson.GsonBuilder
import com.google.gson.internal.LinkedTreeMap
import java.util.*
import kotlin.reflect.KType
import kotlin.reflect.jvm.jvmErasure

class GsonSerializer : Serializer {

    private var currentIndex = 0
    private val rawDataMap = WeakHashMap<RawData, LinkedTreeMap<Any, Any>>()

    private val gson = GsonBuilder()
            .serializeNulls()
            .create()

    override fun <T> serialize(data: T): String {
        return gson.toJson(data)
    }

    private fun getRawObject(): RawData {
        val rawObject = RawData(currentIndex)
        currentIndex++
        return rawObject
    }

    override fun deserializeToRaw(str: String): RawData {
        val rawData: LinkedTreeMap<Any, Any> = gson.fromJson(str)
        val rawObject = getRawObject()
        rawDataMap[rawObject] = rawData
        return rawObject
    }

    @Suppress("UNCHECKED_CAST")
    override fun <T : Any> deserializeRaw(data: RawData, type: KType): T? =
            try {
                val prop = rawDataMap[data] ?: error("")
                gson.fromJson(gson.toJsonTree(prop), type.jvmErasure.java) as T
            } catch (exc: Exception) {
                null
            }

    @Suppress("UNCHECKED_CAST")
    override fun <T : Any> getRawValue(data: RawData, propName: String): T? =
            try {
                val prop = rawDataMap[data]?.get(propName) ?: error("")
                prop as T
            } catch (exc: Exception) {
                null
            }

    @Suppress("UNCHECKED_CAST")
    override fun getRawObject(data: RawData, propName: String): RawData? =
            try {
                val prop = rawDataMap[data]?.get(propName) ?: error("")
                val rawObject = getRawObject()
                rawDataMap[rawObject] = prop as LinkedTreeMap<Any, Any>
                rawObject
            } catch (exc: Exception) {
                null
            }
}
