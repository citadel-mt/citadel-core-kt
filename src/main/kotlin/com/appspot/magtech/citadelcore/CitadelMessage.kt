package com.appspot.magtech.citadelcore

enum class MessageType {
    REQUEST, RESPONSE
}

data class CitadelMessage<T>(val targetId: PluginId, val type: MessageType, val payload: MessagePayload<T>)

data class MessagePayload<T>(val streamId: StreamId, val value: T)

fun <T : Any> packMessage(data: T, type: MessageType, targetId: PluginId, streamId: StreamId): CitadelMessage<T> {
    return CitadelMessage(
            targetId,
            type,
            MessagePayload(
                    streamId,
                    data
            )
    )
}

class SimpleResponse(val status: String)

object EmptyRequest
