package com.appspot.magtech.citadelcore

import com.appspot.magtech.citadelcore.utils.ResponsableEvent
import com.appspot.magtech.citadelcore.utils.Serializer
import com.appspot.magtech.citadelcore.utils.gsonSerializer
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import java.lang.reflect.Proxy
import kotlin.reflect.KClass
import kotlin.reflect.KFunction
import kotlin.reflect.KProperty1
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.isSubtypeOf
import kotlin.reflect.full.memberFunctions
import kotlin.reflect.full.memberProperties
import kotlin.reflect.typeOf

interface CitadelPlugin

const val defaultStreamId = "NOT_SPECIFIED"

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
annotation class PluginInfo(val id: PluginId)

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FUNCTION)
annotation class Sender(val streamId: StreamId = defaultStreamId)

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.PROPERTY)
annotation class Receiver(val streamId: StreamId = defaultStreamId)

@UseExperimental(ExperimentalStdlibApi::class)
val KFunction<*>.isCitadelSenderFunction: Boolean
    get() = parameters.size == 2
            && returnType.isSubtypeOf(typeOf<Deferred<*>>())

@UseExperimental(ExperimentalStdlibApi::class)
val KProperty1<out CitadelPlugin, Any?>.isCitadelReceiver: Boolean
    get() = returnType.isSubtypeOf(typeOf<ResponsableEvent<*, *>>())

@UseExperimental(ExperimentalStdlibApi::class)
inline fun <reified T : CitadelPlugin> citadelPluginFrom(
        channel: CitadelChannel,
        dispatcher: MessageDispatcher,
        serializer: Serializer = gsonSerializer()
) = citadelPluginFrom(channel, dispatcher, serializer, T::class)

@UseExperimental(ExperimentalStdlibApi::class)
@Suppress("UNCHECKED_CAST")
fun <T : CitadelPlugin> citadelPluginFrom(
        channel: CitadelChannel,
        dispatcher: MessageDispatcher,
        serializer: Serializer = gsonSerializer(),
        type: KClass<T>
): T {
    val id = type.findAnnotation<PluginInfo>()?.id
            ?: throw IllegalArgumentException("Can't found Plugin Info")
    val senders: Map<String, Pair<KFunction<*>, StreamId?>> = type.memberFunctions
            .map { func ->
                func to func.findAnnotation<Sender>()?.streamId
            }
            .filter { it.second != null }
            .filter { it.first.isCitadelSenderFunction }
            .associateBy { it.first.name }
    val receivers: Map<String, Pair<KProperty1<T, *>, StreamId?>> = type.memberProperties
            .map { func ->
                func to func.findAnnotation<Receiver>()?.streamId
            }
            .filter { it.second != null }
            .filter { it.first.isCitadelReceiver }
            .associateBy { "get${it.first.name.capitalize()}" }
    return Proxy.newProxyInstance(type.java.classLoader, arrayOf(type.java)) { _, method, args ->
        when {
            senders.contains(method.name) -> {
                val (func, streamId) = senders.getValue(method.name)
                val message = packMessage(args[0], MessageType.REQUEST, id, streamId!!)
                val messageStr = serializer.serialize<CitadelMessage<Any>>(message)
                return@newProxyInstance CoroutineScope(Dispatchers.IO).async {
                    val eventType = func.returnType.arguments[0].type!!

                    data class ReceiveMsd(val value: Any)

                    val receiveChannel = Channel<ReceiveMsd>()
                    val handler: (Any) -> Unit = {
                        CoroutineScope(Dispatchers.IO).launch {
                            receiveChannel.send(ReceiveMsd(it))
                        }
                    }
                    dispatcher.getEvent(id, streamId, eventType) += handler
                    channel.sendMessageAsync(messageStr).await()
                    val result = withTimeoutOrNull(5000) {
                        receiveChannel.receive().value
                    }
                    dispatcher.getEvent(id, streamId, eventType) -= handler
                    if (result != null) {
                        return@async result
                    } else {
                        throw Exception("Response waiting timeout")
                    }
                }
            }
            receivers.contains(method.name) -> {
                val (prop, streamId) = receivers.getValue(method.name)
                val eventType = prop.returnType.arguments[0].type!!
                return@newProxyInstance dispatcher.getResponsableEvent(id, streamId!!, eventType)
            }
            else -> return@newProxyInstance Unit
        }
    } as T
}
