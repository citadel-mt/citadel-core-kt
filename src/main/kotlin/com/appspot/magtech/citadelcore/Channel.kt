package com.appspot.magtech.citadelcore

import com.appspot.magtech.citadelcore.utils.CitadelEvent
import kotlinx.coroutines.Deferred

interface CitadelChannel {

    fun sendMessageAsync(message: String): Deferred<Unit>

    val onReceiveMessageEvent: CitadelEvent<String>
}

interface ClientChannel : CitadelChannel {

    fun scanDevicesAsync(): Deferred<List<ChannelDependentDevice>>

    fun connectAsync(device: ChannelDependentDevice): Deferred<Unit>

    fun disconnectAsync(): Deferred<Unit>
}

interface ServerChannel : CitadelChannel {

    fun initialize()

    val onDeviceConnectedEvent: CitadelEvent<ChannelDependentDevice>

    fun terminate()
}
