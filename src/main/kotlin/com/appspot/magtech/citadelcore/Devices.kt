package com.appspot.magtech.citadelcore

import java.io.Serializable

data class CitadelClient(val type: String, val name: String? = null) : Serializable

data class CitadelServer(val type: String, val name: String? = null) : Serializable

data class ChannelDependentDevice(val id: Int, val name: String) : Serializable
